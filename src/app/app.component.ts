import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Data, Router, NavigationEnd } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map, mergeMap } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title$: Observable<string>;

  constructor(
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.title$ = this.loadTitle();
  }

  loadTitle() {
    return this.router.events.pipe(
      filter((event: any) => event instanceof NavigationEnd),
      map(() => this.activatedRoute),
      map((route: ActivatedRoute) => {
        let lastChild: ActivatedRoute = route;
        while (lastChild.firstChild) {
          lastChild = lastChild.firstChild;
        }
        return lastChild;
      }),
      mergeMap((lastChildRoute: ActivatedRoute) => lastChildRoute.data),
      map((data: Data) => data && data.title)
    );
  }
}
