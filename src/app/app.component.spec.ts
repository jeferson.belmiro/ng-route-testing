import { Component, NgZone } from '@angular/core';
import { TestBed, async, fakeAsync, tick } from '@angular/core/testing';
import { Routes, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';

import { AppComponent } from './app.component';

@Component({
  template: 'dummy',
})
class DummyComponent {}

const routes: Routes = [
  {
    path: 'home',
    component: DummyComponent,
    data: {
      title: 'Home - title',
    },
  },
  {
    path: 'about',
    component: DummyComponent,
    data: {
      title: 'About - title',
    },
  },
];

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes(routes)],
      declarations: [AppComponent, DummyComponent],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should render title', fakeAsync(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const router = TestBed.inject(Router);
    const ngZone = TestBed.inject(NgZone);

    // performs the initial navigation.
    ngZone.run(() => router.navigate(['/home']));

    // init view
    // - execute lifecycle hook ngOnInit
    // - async subscribe to router events
    // - need to be before the route change(next tick)
    fixture.detectChanges();

    // execute route navigation
    // emit events
    // title$ gained value but view has not yet been updated
    tick();

    // update view with subscribed value
    fixture.detectChanges();

    const compiled = fixture.nativeElement.querySelector('h1');
    expect(compiled).toMatchInlineSnapshot(`
      <h1>
        Home - title
      </h1>
    `);
  }));

  it('should update title', async () => {
    const fixture = TestBed.createComponent(AppComponent);
    const router = TestBed.inject(Router);
    const ngZone = TestBed.inject(NgZone);
    const compiled = fixture.nativeElement.querySelector('h1');

    // init view
    // - execute lifecycle hook ngOnInit
    // - async subscribe to router events
    fixture.detectChanges();

    // performs the navigation and wait
    // title$ gained value but view has not yet been updated
    await ngZone.run(() => router.navigate(['/home']));

    // update view with subscribed value
    fixture.detectChanges();

    expect(compiled).toMatchInlineSnapshot(`
      <h1>
        Home - title
      </h1>
    `);

    await ngZone.run(() => router.navigate(['/about']));
    fixture.detectChanges();
    expect(compiled).toMatchInlineSnapshot(`
      <h1>
        About - title
      </h1>
    `);
  });
});
